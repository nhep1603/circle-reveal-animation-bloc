import 'package:bloc/bloc.dart';
import 'package:circle_reveal_animation_bloc/bloc/circle_reveal_bloc.dart';
import 'package:circle_reveal_animation_bloc/phone_with_selected.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'app_bloc_observer.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Bloc.observer = AppBlocObserver();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: BlocProvider(
        create: (context) =>
            CircleRevealBloc(lstPhones: lstPhones)..add(CircleRevealLoaded()),
        child: Home(),
      ),
    );
  }
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  @override
  void initState() {
    context
        .read<CircleRevealBloc>()
        .add(CircleRevealInitedTicker(ticker: this));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          'DBrand Skin selection',
          style: TextStyle(
            color: Colors.black87,
            fontSize: 18.0,
            fontWeight: FontWeight.w700,
          ),
        ),
        leading: BackButton(
          color: Colors.blue,
        ),
        elevation: 0,
      ),
      body: BlocBuilder<CircleRevealBloc, CircleRevealState>(
        builder: (context, state) {
          return state is CircleRevealLoadSuccess 
              ? Column(
                  children: [
                    Expanded(
                      child: Stack(
                        children: [
                          Positioned.fill(
                            child: Image.asset(
                              state.past.phone.image,
                              fit: BoxFit.cover,
                            ),
                          ),
                          Positioned.fill(
                            child: AnimatedBuilder(
                              animation: context.select(
                                  (CircleRevealBloc bloc) =>
                                      bloc.animationController),
                              builder: (context, child) => ClipPath(
                                clipper: SkinClipper(
                                  percent: context.select(
                                      (CircleRevealBloc bloc) =>
                                          bloc.animationController.value),
                                  phone: state.current,
                                ),
                                child: Image.asset(
                                  state.current.phone.image,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Expanded(
                      child: Column(
                        children: [
                          Text(
                            state.current.phone.name,
                            style:
                                Theme.of(context).textTheme.headline5!.copyWith(
                                      color: Colors.black,
                                    ),
                          ),
                          Expanded(
                              child: GridView.builder(
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 4,
                              crossAxisSpacing: 20.0,
                              mainAxisSpacing: 20.0,
                            ),
                            padding: EdgeInsets.all(20.0),
                            itemCount: state.lstPhones.length,
                            itemBuilder: (context, index) {
                              return SkinButton(
                                phone: state.lstPhones[index],
                                onTap: () {
                                  context.read<CircleRevealBloc>().add(
                                        CircleRevealSelected(
                                          current: state.current,
                                          phone: state.lstPhones[index],
                                        ),
                                      );
                                },
                              );
                            },
                          )),
                        ],
                      ),
                    ),
                  ],
                )
              : Text('Don\'t have data.');
        },
      ),
    );
  }
}

class SkinButton extends StatelessWidget {
  const SkinButton({
    Key? key,
    required this.phone,
    required this.onTap,
  }) : super(key: key);

  final PhoneWithSelected phone;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(35.0),
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(
            color: Colors.white,
            width: phone.selected ? 5.0 : 1.0,
          ),
        ),
        child: DecoratedBox(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Color(phone.phone.color),
          ),
        ),
      ),
    );
  }
}

class SkinClipper extends CustomClipper<Path> {
  SkinClipper({
    required this.percent,
    required this.phone,
  });

  final double percent;
  final PhoneWithSelected phone;

  @override
  Path getClip(Size size) {
    final path = Path();
    late Offset center;
    if (phone.debutPosition) {
      center = Offset(0.0, size.height);
    } else {
      center = Offset(size.width, 0.0);
    }

    path.addOval(
      Rect.fromCenter(
        center: center,
        width: size.width * percent,
        height: size.height * percent,
      ),
    );
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => true;
}
