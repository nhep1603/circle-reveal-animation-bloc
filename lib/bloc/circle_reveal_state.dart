part of 'circle_reveal_bloc.dart';

abstract class CircleRevealState extends Equatable {
  const CircleRevealState({
    required this.lstPhones,
    this.current,
    this.past,
  });

  final List<PhoneWithSelected> lstPhones;
  final PhoneWithSelected? current;
  final PhoneWithSelected? past;

  @override
  List<Object> get props => [lstPhones];
}

class CircleRevealInitial extends CircleRevealState {
  CircleRevealInitial({
    required this.lstPhones,
  }) : super(lstPhones: lstPhones);

  final List<PhoneWithSelected> lstPhones;

  @override
  String toString() => 'CircleRevealInitial';

  @override
  List<Object> get props => [lstPhones];
}

class CircleRevealLoadSuccess extends CircleRevealState {
  const CircleRevealLoadSuccess({
    required this.lstPhones,
    required this.current,
    required this.past,
  }) : super(
          lstPhones: lstPhones,
          current: current,
          past: past,
        );
  final List<PhoneWithSelected> lstPhones;
  final PhoneWithSelected current;
  final PhoneWithSelected past;

  @override
  String toString() => 'CircleRevealLoadSuccess';

  @override
  List<Object> get props => [lstPhones, current, past];
}

class CircleRevealLoadFailure extends CircleRevealState {
  const CircleRevealLoadFailure({
    List<PhoneWithSelected> lstPhones = const [],
  }) : super(lstPhones: lstPhones);
}
