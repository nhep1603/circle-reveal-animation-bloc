part of 'circle_reveal_bloc.dart';

abstract class CircleRevealEvent extends Equatable {
  const CircleRevealEvent();

  @override
  List<Object> get props => [];
}

class CircleRevealLoaded extends CircleRevealEvent {}

class CircleRevealInitedTicker extends CircleRevealEvent {
  const CircleRevealInitedTicker({
    required this.ticker,
  });
  final TickerProvider ticker;

  @override
  List<Object> get props => [ticker];
}

class CircleRevealSelected extends CircleRevealEvent {
  const CircleRevealSelected({
    required this.phone,
    required this.current,
  });
  final PhoneWithSelected phone;
  final PhoneWithSelected current;

  @override
  List<Object> get props => [phone, current];
}
