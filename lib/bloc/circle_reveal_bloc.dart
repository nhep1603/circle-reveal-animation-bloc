import 'package:bloc/bloc.dart';
import 'package:circle_reveal_animation_bloc/phone_with_selected.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

part 'circle_reveal_event.dart';
part 'circle_reveal_state.dart';

class CircleRevealBloc extends Bloc<CircleRevealEvent, CircleRevealState> {
  CircleRevealBloc({
    required this.lstPhones,
  }) : super(CircleRevealInitial(
          lstPhones: lstPhones,
        )) {
    on<CircleRevealLoaded>(_onLoaded);
    on<CircleRevealInitedTicker>(_onInitedTicker);
    on<CircleRevealSelected>(_onSelected);
  }

  final List<PhoneWithSelected>
      lstPhones; //! Thực tế sẽ là Repository trả về 1 List empty hoặc có elements
  late AnimationController animationController;

  @override
  Future<void> close() {
    animationController.dispose();
    return super.close();
  }

//! Check lstPhones có element hay không
  _onLoaded(CircleRevealLoaded event, Emitter<CircleRevealState> emit) {
    if (state.lstPhones.length != 0) {
      emit(CircleRevealLoadSuccess(
        lstPhones: lstPhones,
        current: lstPhones.first,
        past: lstPhones.first,
      ));
    } else {
      emit(CircleRevealLoadFailure());
    }
  }

  _onInitedTicker(
      CircleRevealInitedTicker event, Emitter<CircleRevealState> emit) {
    animationController = AnimationController(
      vsync: event.ticker,
      duration: const Duration(milliseconds: 400),
      upperBound: 3.5,
    );
  }

  _onSelected(CircleRevealSelected event, Emitter<CircleRevealState> emit) {
    List<PhoneWithSelected> list = [];
    for (var i = 0; i < state.lstPhones.length; i++) {
      final condition = state.lstPhones[i] == event.phone;
      final phone = state.lstPhones[i].copyWith(selected: condition);
      list.add(phone);
    }
    animationController.forward(from: 0.0);
    emit(CircleRevealLoadSuccess(
      lstPhones: list,
      current: event.phone,
      past: event.current,
    ));
  }
}
