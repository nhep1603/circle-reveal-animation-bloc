//! object for data
class Phone {
  const Phone({
    required this.name,
    required this.color,
    required this.image,
  });
  final String name;
  final int color;
  final String image;
}
