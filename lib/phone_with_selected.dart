import 'phone.dart';

//! Object for UI
class PhoneWithSelected {
  const PhoneWithSelected({
    required this.phone,
    required this.selected,
    required this.debutPosition,
  });

  final Phone phone;
  final bool selected;
  final bool debutPosition;

  PhoneWithSelected copyWith({
    Phone? phone,
    bool? selected,
    bool? debutPosition,
  }) {
    return PhoneWithSelected(
      phone: phone ?? this.phone,
      selected: selected ?? this.selected,
      debutPosition: debutPosition ?? this.debutPosition,
    );
  }
}

//! the first element : selected : true
//! the even elements : debutPosition : true
//! the odd elements : debutPosition : false
const lstPhones = [
  PhoneWithSelected(
    phone: Phone(
      name: 'Sunset Red',
      color: 0xFFE96B6A,
      image: 'assets/images/red.jpg',
    ),
    selected: true,
    debutPosition: true,
  ),
  PhoneWithSelected(
    phone: Phone(
      name: 'Sunrise Orange',
      color: 0xFFFE9968,
      image: 'assets/images/orange.jpg',
    ),
    selected: false,
    debutPosition: false,
  ),
  PhoneWithSelected(
    phone: Phone(
      name: 'Mellow Yellow',
      color: 0xFFFFD387,
      image: 'assets/images/yellow.jpg',
    ),
    selected: false,
    debutPosition: true,
  ),
  PhoneWithSelected(
    phone: Phone(
      name: 'Seafoam Green',
      color: 0xFF6CE5B1,
      image: 'assets/images/green.jpeg',
    ),
    selected: false,
    debutPosition: false,
  ),
  PhoneWithSelected(
    phone: Phone(
      name: 'Dark black',
      color: 0xFF000000,
      image: 'assets/images/black.jpg',
    ),
    selected: false,
    debutPosition: true,
  ),
  PhoneWithSelected(
    phone: Phone(
      name: 'Kind of purple',
      color: 0xFF98A2DF,
      image: 'assets/images/purple.jpg',
    ),
    selected: false,
    debutPosition: false,
  ),
  PhoneWithSelected(
    phone: Phone(
      name: 'Off pink',
      color: 0xFFEBB9D2,
      image: 'assets/images/pink.jpg',
    ),
    selected: false,
    debutPosition: true,
  ),
  PhoneWithSelected(
    phone: Phone(
      name: 'Silver',
      color: 0xFFD6D9D2,
      image: 'assets/images/white.jpg',
    ),
    selected: false,
    debutPosition: false,
  ),
];
