# circle_reveal_animation_bloc
- Bloc + ObserverBloc
- AnimatedBuilder()
- ClipPath() + CustomClipper<Path>
- GridView.builder() + SliverGridDelegateWithFixedCrossAxisCount()

![circle_reveal_animation_bloc](circle_reveal_animation_bloc.mp4)
